Despliegue del proyecto en el [enlace](https://amendeza97.gitlab.io/demofront)

Despliegue del proyecto en el [enlace](https://amendeza97.gitlab.io/demofront)

## Instalaciones necesarias

Para los siguientes pasos es necesario usar la terminal y escribir los siguientes comandos.

### Validar versión de nodeJS
> node -v

Si no se encuentra instaldo, descargue el instalador desde el siguiente enlance [nodeJS-v18 installer](https://nodejs.org/download/release/v18.19.1/).

### Validar versión de npm
> npm -v

Luego de asegurarse de contar con la versión de nodeJS 18.X.X, la versión de npm debe estar alrededor de 10.2.4.

### Validar versión de ng
> ng -v

Si no se encuentra instaldo, escriba el siguiente comando.
> npm install -g @angular/cli

Si su sistema operativo es Windows, escriba el siguiente comando.
> Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy RemoteSigned

### Validar versión de git
> git --version

Si no se encuentra instaldo, descargue el instalador desde el siguiente enlance [git installer](https://git-scm.com/downloads).


## Instalación del proyecto

### Clonar el repositorio
> git clone https://gitlab.com/amendeza97/demofront.git

> cd demofront

### Instalar dependencias
> npm install

### Ejecturar proyecto en local
> ng serve --open

Debe abrirse una ventana del navegador y visualizarse el proyecto.


## Creación de repositorio propio.

### Crear un repositorio propio en gilab con el nombre demofront

### Ajustar Gitlab pages

Activar la configuracion de Gitlab pages en Settings > General > Visibility, project features, permissions > Pages.

**Nota:** Asegurar la activación del check de Pages.

### Renombrar la URL de origin
> git remote set-url origin <url del repositorio propio>

### Desplegar en repositorio propio
> git push origin develop

### Validar despliegue
Abrir ventana del navegador e ingresar a https://<nombre usuario>.gitlab.io/<nombre-repo>
